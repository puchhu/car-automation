#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#define BLYNK_PRINT Serial
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

static const int RXPin = 4, TXPin = 5;       // GPIO 4=D2(conneect Tx of GPS) and GPIO 5=D1(Connect Rx of GPS)
static const uint32_t GPSBaud = 9600;       //if Baud rate 9600 didn't work in your case then use 4800

TinyGPSPlus gps;                          // The TinyGPS++ object
WidgetMap myMap(V0);                     // V0 for virtual pin of Map Widget

SoftwareSerial ss(RXPin, TXPin);       // The serial connection to the GPS device

BlynkTimer timer;

float spd;                          //Variable  to store the speed
float sats;                        //Variable to store no. of satellites response
String bearing;                   //Variable to store orientation or direction of GPS

char auth[] = "2tdyr93uhvXm6ohJ3glpEnP000XFzHJW";            
char ssid[] = "Raj";                                       
char pass[] = "raj@00321";                                      
//unsigned int move_index;        // moving index, to be used later
unsigned int move_index = 1;     // fixed location for now

#include <Serial_CAN_Module.h>
Serial_CAN can;
#define STANDARD_CAN_11BIT  1       // That depands on your car. some 1 some 0. 
#define can_tx  2           // tx of serial can module connect to D2
#define can_rx  3           // rx of serial can module connect to D3
char c;
//HEX data;
int loop_count=0;
#define PID_ENGIN_PRM       0x0C
#define PID_VEHICLE_SPEED   0x0D
#define PID_COOLANT_TEMP    0x05
#if STANDARD_CAN_11BIT
#define CAN_ID_PID          0x7DF
#else
#define CAN_ID_PID          0x18db33f1
#endif

unsigned char PID_INPUT;
unsigned char getPid    = 0;

#if STANDARD_CAN_11BIT
unsigned long mask[4] = 
                        {
                          0, 0x7FC,                // ext, maks 0
                          0, 0x7FC,                // ext, mask 1
                        };

unsigned long filt[12] = 
                         {
                           0, 0x7E8,                // ext, filt 0
                           0, 0x7E8,                // ext, filt 1
                           0, 0x7E8,                // ext, filt 2
                           0, 0x7E8,                // ext, filt 3
                           0, 0x7E8,                // ext, filt 4
                           0, 0x7E8,                // ext, filt 5
                          };

#else
unsigned long mask[4] =
                        {
                          1, 0x1fffffff,               // ext, maks 0
                          1, 0x1fffffff,
                        };
 
unsigned long filt[12] =
                         {
                           1, 0x18DAF110,                // ext, filt
                           1, 0x18DAF110,                // ext, filt 1
                           1, 0x18DAF110,                // ext, filt 2
                           1, 0x18DAF110,                // ext, filt 3
                           1, 0x18DAF110,                // ext, filt 4
                           1, 0x18DAF110,                // ext, filt 5
                          };
#endif

void set_mask_filt()
{
  if(can.setMask(mask)){
    Serial.println("set mask ok");
  }
  else{
    Serial.println("set mask fail");
  }
  if(can.setFilt(filt)){
    Serial.println("set filt ok");
  }
  else{
    Serial.println("set filt fail");
  }
}

void sendPid(unsigned char __pid){
  unsigned char tmp[8] = {0x02, 0x01, __pid, 0, 0, 0, 0, 0};
    Serial.print("SEND PID: 0x");
    Serial.println(__pid, HEX);

    #if STANDARD_CAN_11BIT
      can.send(CAN_ID_PID, 0, 0, 8, tmp);   // SEND TO ID:0X55
    #else
      can.send(CAN_ID_PID, 1, 0, 8, tmp);   // SEND TO ID:0X55
    #endif
}

void setup(){
  Serial.begin(9600);
  ss.begin(GPSBaud);
  Blynk.begin(auth, ssid, pass);
  timer.setInterval(5000L, checkGPS);     // every 5s check if GPS is connected, only really needs to be done once
  
  can.begin(can_tx, can_rx, 9600);      // tx, rx
  // set baudrate of CAN Bus to 500Kb/s
  if(can.canRate(CAN_RATE_500)){
    Serial.println("set can rate ok");
  }
  else{
    Serial.println("set can rate fail");
  }
  set_mask_filt();
  Serial.println("begin");
}

void checkGPS(){
  if (gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS detected: check wiring."));
      Blynk.virtualWrite(V4, "GPS ERROR");                 // Value Display widget  on V4 if GPS not detected
  }
}


void loop(){
  while (ss.available() > 0){
    if (gps.encode(ss.read()))
      displayInfo();
  }
  Blynk.run();
  timer.run();
  
  loop_count=loop_count+1;
  delay(50);
  taskCanRecv();
  taskDbg();
  if(getPid){           // GET A PID
    getPid = 0;
    sendPid(PID_INPUT);
    PID_INPUT = 0;
  }
}

void displayInfo(){ 
  if (gps.location.isValid()) {
    float latitude = (gps.location.lat());          //Storing the Lat. and Lon. 
    float longitude = (gps.location.lng()); 
    
    Serial.print("LAT:  ");
    Serial.println(latitude, 6);                  // float to x decimal places
    Serial.print("LONG: ");
    Serial.println(longitude, 6);
    Blynk.virtualWrite(V1, String(latitude, 6));   
    Blynk.virtualWrite(V2, String(longitude, 6));  
    myMap.location(move_index, latitude, longitude, "GPS_Location");
    spd = gps.speed.kmph();                     //get speed
       Blynk.virtualWrite(V3, spd);
       
       sats = gps.satellites.value();         //get number of satellites
       Blynk.virtualWrite(V4, sats);

       bearing = TinyGPSPlus::cardinal(gps.course.value());     // get the direction
       Blynk.virtualWrite(V5, bearing);               
  }
}

void taskCanRecv(){
  unsigned char len = 0;
  unsigned long id  = 0;
  unsigned char buf[8];
  if(can.recv(&id, buf)){      // check if get data
    Serial.println("\r\n------------------------------------------------------------------");
    Serial.print("Get Data From id: 0x");
    Serial.println(id, HEX);
    for(int i = 0; i<len; i++){             // print the data
      Serial.print("0x");
      Serial.print(buf[i], HEX);
      Serial.print("\t");
    }  
    Serial.print(buf[2]);       
    Serial.println();
  }
}

void taskDbg(){
  char c='g';
  if (loop_count==30){
    c='4';
  }
  if (loop_count==60){
    c='5';
  }
  if (loop_count==90){
    c='c';
  }
  if (loop_count==120){
    c='d';
  }
  if (loop_count==150){
    c='f';
  }
  if (loop_count==180){
    loop_count=0;
    c='6';
  }
  if(c>='0' && c<='9'){
    PID_INPUT *= 0x10;
    PID_INPUT += c-'0';
    getPid = 1;
    c='g';                //just a random charecter
  }
  else if(c>='A' && c<='F'){
    PID_INPUT *= 0x10;
    PID_INPUT += 10+c-'A';
    getPid = 1;
    c='g';            //just a random charecter
  }
  else if(c>='a' && c<='f'){
    PID_INPUT *= 0x10;
    PID_INPUT += 10+c-'a';
    getPid = 1;
    c='g';          //just a random charecter
  }
}
